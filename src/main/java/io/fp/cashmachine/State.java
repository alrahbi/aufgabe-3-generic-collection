package io.fp.cashmachine;

public enum State 
{
	READY,
	CARD_INSERTED,
	PIN_CORRECT,
	PIN_WRONG
}
