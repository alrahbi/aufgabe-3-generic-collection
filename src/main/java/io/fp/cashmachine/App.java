package io.fp.cashmachine;

import java.util.LinkedList;

public class App {

	public static void main(String[] args) throws IllegalWithdrawException, CashMachineException {
 
		AccountDetails ad1 = new AccountDetails("DE334578900765", "64050000");
		Account<Euro> acc1 = new Account<Euro>(ad1, 200, 150, new Euro(1, "€"), 1234);

		CashCard cashCard1 = new CashCard(ad1);
		
		AccountDetails ad2 = new AccountDetails("US355778554333", "6535000");
		Account<Dollar> acc2 = new Account<Dollar>(ad2, 200, 150, new Dollar(1, "$"), 1234);
		
		CashCard cashCard2 = new CashCard(ad2);

		LinkedList<Account<? extends Currency>> accList = new LinkedList<>();

		accList.add(acc1);
		accList.add(acc2);

		CashMachine cashMachine = new CashMachine(accList);

		cashMachine.insertCashCard(cashCard1);
		cashMachine.enter(1234);
		cashMachine.accountStatement();
		cashMachine.ejectCashCard();

		System.out.println();
		
		cashMachine.insertCashCard(cashCard2);
		cashMachine.enter(1234);
		cashMachine.accountStatement();
		cashMachine.ejectCashCard();

		//Transaction
		
		Transaction transaction = new Transaction(acc1, "US355778554333", 50);

		cashMachine.insertCashCard(cashCard1);
		cashMachine.enter(1234);
		cashMachine.pushTransaction(transaction);
		cashMachine.proceedTransactionQueue();

		System.out.println();

		cashMachine.accountStatement();
		cashMachine.ejectCashCard();

		System.out.println();

		cashMachine.insertCashCard(cashCard2);
		cashMachine.enter(1234);
		cashMachine.accountStatement();
		cashMachine.ejectCashCard();

		
	
	}
}