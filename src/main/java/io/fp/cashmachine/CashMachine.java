package io.fp.cashmachine;


import java.util.LinkedList;
import java.util.Queue;

import io.fp.cashmachine.Account;
import io.fp.cashmachine.CashCard;
import io.fp.cashmachine.CashMachineException;
import io.fp.cashmachine.State;

public class CashMachine {

	private LinkedList<Account<? extends Currency>> accountList;
	private CashCard cashCard;
	private Account<? extends Currency> selectedAccount;
	private State state;
	private int pinSecurity;
	private Queue<Transaction> transactionQueue;

	public CashMachine(LinkedList<Account<? extends Currency>> accs) {
		this.accountList = accs;
		state = State.READY;
		transactionQueue = new LinkedList<>();
	}

	public void insertCashCard(CashCard cashCard) throws CashMachineException {
		switch (state) {
		case CARD_INSERTED:
		case PIN_WRONG:
		case PIN_CORRECT:
			throw new CashMachineException("Cash Machine is not ready for this operation.");
		case READY:

			this.cashCard = cashCard;

			selectedAccount = selectAccount(this.cashCard.getDetails().getIban());
			if (selectedAccount == null) {
				this.cashCard = null;
				throw new CashMachineException("Account is not available at this machine.");
			}
			state = State.CARD_INSERTED;
			System.out.println("New status is: " + state);

			pinSecurity = 0;
			break;

		default:
			throw new CashMachineException("Cash Machine does not support this state.");

		}
	}

	private Account<? extends Currency> selectAccount(String iban) {
		for (Account<? extends Currency> acc : accountList) {
			if (acc != null) {
				if (acc.getDetails().getIban().equals(iban)) {
					return acc;
				}
			}
		}
		return null;
	}

	public void withdraw(double amount) throws CashMachineException {
		switch (state) {
		case CARD_INSERTED:
		case READY:
		case PIN_WRONG:
			throw new CashMachineException("Cash Machine is not ready for this operation.");
		case PIN_CORRECT:
			try {
				selectedAccount.withdraw(amount);
			} catch (IllegalWithdrawException e) {
				throw new CashMachineException(
						"Cash Machine is not ready for this operation because: " + e.getMessage(), e);
			}
			break;
		default:
			break;

		}
	}

	public void accountStatement() throws CashMachineException {
		switch (state) {
		case CARD_INSERTED:
		case PIN_CORRECT:
		case PIN_WRONG:
			System.out.println("Kontoinformationen");
			System.out.println("Kontonummer: " + selectedAccount.getDetails().getIban());
			System.out.println("Guthaben: " + selectedAccount.getBankDeposit() + " "
					+ selectedAccount.getCurrency().getCurrencyCode());
			System.out.println("Dispokredit: " + selectedAccount.getOverdraft() + " "
					+ selectedAccount.getCurrency().getCurrencyCode());
			break;
		case READY:
			throw new CashMachineException("Cash Machine is not ready for this operation.");
		default:
			break;

		}
	}

	public void ejectCashCard() throws CashMachineException {
		switch (state) {
		case CARD_INSERTED:
		case PIN_CORRECT:
		case PIN_WRONG:
			selectedAccount = null;
			cashCard = null;
			state = State.READY;
			System.out.println("New status is: " + state);
			break;
		case READY:
			throw new CashMachineException("Cash Machine is not ready for this operation.");
		default:
			break;

		}
	}

	public void enterPin(int pin) throws CashMachineException {
		switch (state) {
		case CARD_INSERTED:
		case PIN_WRONG:
			enter(pin);
			break;
		case PIN_CORRECT:
		case READY:
			throw new CashMachineException("Cash Machine is not ready for this operation.");
		default:
			throw new CashMachineException("Cash Machine does not support this state.");
		}
	}

	public void enter(int pin) throws CashMachineException {
		pinSecurity++;
		if (selectedAccount.getPin() == pin) {
			state = State.PIN_CORRECT;
			System.out.println("New status is: " + state);
		} else {
			if (pinSecurity == 3) {
				System.out.println("Pin three times false. Card was confiscated.");
				ejectCashCard();
				return;
			}
			state = State.PIN_WRONG;
			System.out.println("New status is: " + state);
		}
	}

	public void pushTransaction(Transaction transaction) {
		transactionQueue.add(transaction);
	}

	public void proceedTransactionQueue() throws CashMachineException {
		while (!transactionQueue.isEmpty()) {
			Transaction trans = transactionQueue.poll();

			Account<? extends Currency> source = selectAccount(trans.getSourceAccount().getDetails().getIban());
			if (source == null) {
				throw new CashMachineException("Source Account not detected!");
			}

			Account<? extends Currency> target = selectAccount(trans.getTargetIban());
			if (target == null) {
				throw new CashMachineException("Target Account not detected!");
			}

			try {
				source.withdraw(trans.getAmount());
			} catch (IllegalWithdrawException e) {
				throw new CashMachineException(
						"Cash Machine is not ready for this operation because: " + e.getMessage(), e);
			}
			target.deposit(trans.getAmount());
		}
	}

	public State getState() {
		return state;
	}

	public CashCard getCashCard() {
		return cashCard;
	}

	public Account<? extends Currency> getSelectedAccount() {
		return selectedAccount;
	}
}
